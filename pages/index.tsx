/* eslint-disable react/jsx-key */
import produce from "immer";
import { useCallback, useEffect, useRef, useState, PropsWithChildren, ReactElement } from "react";

type Cell = {
    color: string;
};

type Map = Cell[][]

type Coords = { 
    i: number, j: number 
};

type Change = {
    // change the cell at specified coords to new color
    coords: Coords,
    color: string,

    // can be used for animating the changes
    step: number,
};

let maxMoves = 54;

function generateMap(width: number, height: number): Map {
    let res: Cell[][] = [];
    let colors = ['red', 'green', 'blue', 'yellow', 'magenta', 'cyan', 'orange'];

    for(let i=0; i<height; i++) {
        res.push([]);
        for(let j=0; j<width; j++) {
            let color = colors[Math.floor(Math.random()*colors.length)];
            res[i].push({ color: color });
        }
    }

    return res;
}

function floodFill(map: Cell[][], newColor: string): Change[] {
    type QEntry = Coords & { step: number };
    let Q: QEntry[] = [];
    let V: Set<string> = new Set();

    let originalColor = map[0][0].color;
    if(newColor === originalColor) return [];

    let canAddCoords = (c?: QEntry): boolean => {
        if(!c) return false;
        let coords = { i: c.i, j: c.j };
        if(V.has(JSON.stringify(coords))) return false;
        return (map[c.i][c.j].color === originalColor);
    };

    let left = (c: QEntry): QEntry | undefined => {
        if(c.j === 0) return undefined;

        let res = { i: c.i, j: c.j - 1, step: c.step + 1 };
        if(!canAddCoords(res)) return undefined;

        return res;
    };

    let right = (c: QEntry): QEntry | undefined => {
        if(c.j === map[0].length - 1) return undefined;

        let res = { i: c.i, j: c.j + 1, step: c.step + 1 };
        if(!canAddCoords(res)) return undefined;

        return res;
    };

    let top = (c: QEntry): QEntry | undefined => {
        if(c.i === 0) return undefined;

        let res = { i: c.i - 1, j: c.j, step: c.step + 1 };
        if(!canAddCoords(res)) return undefined;

        return res;
    };
    let bottom = (c: QEntry): QEntry | undefined => {
        if(c.i === map.length - 1) return undefined;

        let res = { i: c.i + 1, j: c.j, step: c.step + 1 };
        if(!canAddCoords(res)) return undefined;

        return res;
    };

    let addToQueue = (ent?: QEntry) => {
        if(!ent) return;
        Q.push(ent);
        V.add(JSON.stringify({ i: ent.i, j: ent.j}));
    }

    addToQueue({ i: 0, j: 0, step: 0 })

    let changes: Change[] = [];

    while(Q.length !== 0) {
        let curr = Q.shift();
        if(!curr) break;

        changes.push({
            coords: { i: curr.i, j: curr.j },
            step: curr.step,
            color: newColor,        
        });
        
        addToQueue(left(curr));
        addToQueue(right(curr));
        addToQueue(top(curr));
        addToQueue(bottom(curr));
    }

    return changes;
}

function useMap() {
    let [map, setMap] = useState(() => generateMap(26, 26));
    let [moves, setMoves] = useState(0);

    let changeToColor = useCallback((color: string) => {
        if(map[0][0].color === color) return [];
        setMoves(moves+1);

        return floodFill(map, color);
    }, [map, moves]);

    let applyChanges = (changes: Change[]) => {
        setMap(produce(map => {
            changes.forEach(ch => {
                map[ch.coords.i][ch.coords.j].color = ch.color;
            });
        }));
    };

    let allTheSame = (): boolean => {
        let color = map[0][0].color;
        return map.every(row => row.every(cell => cell.color === color));
    }

    let status: "win" | "lose" | undefined = undefined;
    if(moves > maxMoves) status = "lose";
    else if(allTheSame()) status = "win";

    let restart = () => {
        setMap(() => generateMap(26, 26));
        setMoves(0);
    }

    return { map, moves, status, changeToColor, applyChanges, restart };
}

function MapDisplay(props: PropsWithChildren<{ map: Map, onClick?: (c: Coords) => void }>): ReactElement {
    let canvasRef = useRef<HTMLCanvasElement>(null);

    let { map, onClick } = props;

    useEffect(() => {
        let ctx = canvasRef.current?.getContext("2d");
        if(!ctx) return;

        let height = map.length;
        let width = map[0].length;

        let widthPx = canvasRef.current!.width / width;
        let heightPx = canvasRef.current!.height / height;

        ctx.strokeStyle = "rgba(0, 0, 0, 30%)";
        ctx.lineWidth = 1;

        for(let i=0; i<height; i++) {
            for(let j=0; j<width; j++) {
                ctx.fillStyle = map[i][j].color;
                ctx.fillRect(j * widthPx, i * heightPx, widthPx, heightPx);
                ctx.strokeRect(j * widthPx, i * heightPx, widthPx, heightPx);
            }
        }
    }, [map]);

    let canvasClicked = (evt: any) => {
        let x = evt.nativeEvent.offsetX;
        let y = evt.nativeEvent.offsetY;

        let height = map.length;
        let width = map[0].length;

        let widthPx = canvasRef.current!.width / width;
        let heightPx = canvasRef.current!.height / height;

        let i = Math.floor(y / heightPx);
        let j = Math.floor(x / widthPx);

        if(onClick) onClick({ i, j });
    };

    return <canvas ref={canvasRef} width={500} height={500} onClick={canvasClicked}></canvas>;
}

export default function Home() {
    let { map, moves, status, changeToColor, applyChanges, restart } = useMap();
    let animationInProgress = useRef(false);

    let msg = "";
    if(status === "lose") msg = "You Lose!"
    else if(status === "win") msg = "You win!"

    let onMapClick = ({ i, j }: Coords) => {
        // TODO: Insta-complete the animation if a second click occurs while the animation is running
        if(animationInProgress.current) return;

        let changes = changeToColor(map[i][j].color);

        let doStep = (step: number) => {
            let changesForCurrentStep = changes.filter(ch => ch.step === step);
            if(changesForCurrentStep.length === 0) {
                animationInProgress.current = false;
                return;
            }

            applyChanges(changesForCurrentStep);
            requestAnimationFrame(() => doStep(step+1));
        };
        
        animationInProgress.current = true;
        doStep(0);
    };

    return <>
        <h1>Floodit</h1>
        <MapDisplay map={map} onClick={onMapClick} />
        <p>{moves}/{maxMoves}</p>
        <p>{msg}</p>
        {status && <p><button onClick={restart}>New Game?</button></p>}
    </>;
}
